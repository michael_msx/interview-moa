<?php
/**
 * Created by PhpStorm.
 * User: Shixuan
 * Date: 2014/11/13
 * Time: 17:13
 */

include 'ganon.php';

function html_to_obj($html){

    // init obj array
    $obj = array();
    $attributes_array = array();
    $children_array =array();

    // special tags which could not have text value , avoid from such as '\n   '.
    $s_array = array('html','head','body','div');

    // check attribute , from example json file, add an attribute to style tag.
    if($html->attributeCount() > 0){
        foreach($html->attributes as $attribute => $value) {
            $attributes_array[$attribute] = $value;
        }
        $obj['attributes'] = $attributes_array;
    }elseif($html->tag == 'style'){
        $attributes_array['type'] = 'text/css';
        $obj['attributes'] = $attributes_array;
    }

    // check child nodes. if child node is text node , directly show value.

    if($html->childCount() > 0 ){
        foreach($html->children  as $child) {
            if($child->parent->getAttribute('class') == 'synopsis'){
                $obj['value'] = $child->getInnerText();
            }
            elseif($child->isText()){
                if(!in_array( $child->parent->tag , $s_array)){
                    $obj['value'] = $child->getInnerText();
                }
            }
            else{
                if($child->tag == $child->parent->tag){
                    $tagName =  $child->tag .'#'.$child->id;
                }else{
                    $tagName =  $child->tag;
                }
                $children_array[$tagName] = html_to_obj($child);
                $obj['child_nodes'] = $children_array;

            }
        }
    }

    return $obj;
}