<?php
/**
 * Created by PhpStorm.
 * User: Shixuan
 * Date: 2014/11/13
 * Time: 9:55
 */

include 'ganon.php';
include 'html_to_obj.php';


    // read html file from server.
    $url = 'http://testing.moacreative.com/job_interview/php/index.html';
    $doc = new DOMDocument();
    $doc->loadHTMLFile($url);

    // fixed the <a> tag error of the html code.
    $tags= $doc->getElementsByTagName('a');
    foreach ($tags as $tag) {
         if (!$tag->hasAttributes()) {
            while ($tag->childNodes->length > 0) {
                $child = $tag->childNodes->item(0);
                $tag->removeChild($child);
                $tag->parentNode->appendChild($child);
            }
             $tag->parentNode->removeChild($tag);
         }
    }

    // create ganon lib dom obj from new html code.
    $str =  $doc->saveHTML();
    $root = str_get_dom($str);
    $html = $root('html',0);

    // using html_to_obj method to covert html to php array.
    $jo = html_to_obj($html);

    // using json_encode method to covert php array to json code.
    $json_str = json_encode( array($html->tag => $jo),JSON_UNESCAPED_SLASHES);
    echo $json_str;
    // output json code to file : data.json
    file_put_contents('data.json',$json_str);







